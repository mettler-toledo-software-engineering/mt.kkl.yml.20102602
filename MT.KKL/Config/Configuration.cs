﻿using MT.Singularity.Components;
using MT.Singularity.Platform.Configuration;
using System.ComponentModel;

namespace MT.KKL.Config
{
    [Component]
    public class Configuration : ComponentConfiguration
    {
        public Configuration()
        {
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(@"C:\Packages\MT.KKL\Data")]
        public virtual string DataSource
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        // [DefaultValue("COM5")]
        public virtual string BarcodeReaderComPort
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("")]
        public virtual string UsbUpdateResult
        {
           get { return (string)GetLocal(); }
           set { SetLocal(value); }
        }
    }
}