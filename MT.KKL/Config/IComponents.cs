﻿using MT.Singularity.Platform.Configuration;

namespace MT.KKL.Config
{
    internal interface IComponents : IConfigurable<Configuration>
    {
    }
}