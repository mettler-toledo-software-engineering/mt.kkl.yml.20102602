﻿using System;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace MT.KKL.Data.Database
{
    public interface IDataBaseConnection<T>
    {
        Task<int> SaveDataAsync(T dataEntry);
        Task<bool> UpdateDataAsnyc(T dataEntry);
        Task<bool> DeleteDataAsync(T dataEntry);
        Task<List<T>> GetAllDataEntriesAsync();
        Task<T> GetEntryByIdAsync(int id);
        Task<List<T>> GetEntriesByParameterAsync(string parameterName, object parameterValue);
        Task<bool> DeleteDataOlderThenAsync(int months);
        Task<List<T>> GetDataBetweenTwoDatesAsync(DateTime start, DateTime end);
    }
}
