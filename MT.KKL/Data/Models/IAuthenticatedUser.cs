﻿namespace DataManager.Models
{
    public interface IAuthenticatedUser
    {
        string Access_Token { get; set; }
    }
}