﻿namespace DataManager.Models
{
    public interface IApiProdConfirmationData
    {
        string HU_REPROCESSED { get; set; }
        string LGNUM { get; set; }
        string MATNR { get; set; }
        string MEINH { get; set; }
        decimal MENGE { get; set; }
        int POBJID { get; set; }
        string PRINTER { get; set; }
        string TEAMID { get; set; }
        int VERID { get; set; }
        string WERKS { get; set; }
    }
}