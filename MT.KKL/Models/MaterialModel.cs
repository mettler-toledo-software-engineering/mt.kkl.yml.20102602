﻿using System;
using Dapper.Contrib.Extensions;
using MT.KKL.Logic;

namespace MT.KKL.Models
{
    [Table("MaterialModels")]
    public class MaterialModel
    {
        [Key]
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public DateTime LastChanged { get; set; }
        public string Name { get; set; }
        public double MaxWeight { get; set; }
        public double HeatingValue { get; set; }
        public double Weight { get; set; }
        public MaterialType Type { get; set; }

        public MaterialModel()
        {
        }

        public MaterialModel(string name)
        {
            Name = name;
        }

        public override string ToString()
        {
            return $"{Name}";
        }
    }
}
