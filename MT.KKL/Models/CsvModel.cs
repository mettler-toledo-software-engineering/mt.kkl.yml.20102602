﻿namespace MT.KKL.Models
{
    public class CsvModel
    {
        public string UserKey { get; set; }
        public string MaterialCode { get; set; }
        public double TareWeight { get; set; }
        public double NetWeight { get; set; }
        public double GrossWeight { get; set; }
        public string Unit { get; set; }

        public CsvModel()
        {
        }

        public CsvModel(string userKey, string materialKey, double tare, double netWeight, double grossWeight, string unit)
        {
            UserKey = userKey;
            MaterialCode = materialKey;
            TareWeight = TareWeight;
            NetWeight = netWeight;
            GrossWeight = grossWeight;
            Unit = unit;
        }
    }
}
