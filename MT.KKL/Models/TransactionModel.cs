﻿using System;
using Dapper.Contrib.Extensions;

namespace MT.KKL.Models
{
    [Table("TransactionModels")]
    public class TransactionModel
    {
        [Key]
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }

        public int UserId { get; set; }
        public int MaterialId { get; set; }
        [Computed]
        public MaterialModel Material { get; private set; }
        [Computed]
        public double TareWeight { get; set; }
        public double NetWeight { get; set; }
        public double GrossWeight { get; set; }
        public string Unit { get; set; }
        public bool IsExported { get; set; }

        public TransactionModel()
        {
        }
        
        public void SetMaterial(MaterialModel material)
        {
            MaterialId = material.Id;
            Material = material;
        }
    }
}
