﻿using log4net;
using MT.Singularity.Data;
using MT.Singularity.Logging;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;

namespace MT.KKL.ViewModels
{
    public class OverviewViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        private static readonly ILog _log = Log4NetManager.ApplicationLogger;

        private DelegateCommand _goBackCommand;

        public OverviewViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;
        }

        #region Buttons
        
        #region GoBack

        public ICommand GoBack => _goBackCommand = new DelegateCommand(DoGoBack);

        private void DoGoBack()
        {
            _homeNavigationFrame.Back();
        }

        #endregion

        #endregion
    }
}
