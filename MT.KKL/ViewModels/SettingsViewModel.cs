﻿using MT.KKL.Logic;
using MT.KKL.Models;
using MT.KKL.View.ChildViews;
using MT.KKL.ViewModels.ChildViewModels;
using MT.KKL.Views;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;
using System;
using System.Collections.Generic;
using System.IO;

namespace MT.KKL.ViewModels
{
    public class SettingsViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        private Settings _currentSettings;

        public SettingsViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;

            _currentSettings = Settings.Current;
            LoadFields();
        }

        private void LoadFields()
        {
            FileLocation = _currentSettings.FileLocation;
            Interval = _currentSettings.Interval;
        }

        private void SaveFields()
        {
            _currentSettings.FileLocation = FileLocation;
            _currentSettings.Interval = Interval;

            _currentSettings.Save();
        }

        private bool HasChanged()
        {
            return (FileLocation != _currentSettings.FileLocation) || (Interval != _currentSettings.Interval);
        }

        #region FileLocation
        private string _fileLocation;

        public string FileLocation
        {
            get
            {
                return _fileLocation;
            }
            set
            {
                if (value != _fileLocation)
                {
                    _fileLocation = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Interval
        private int _interval;

        public int Interval
        {
            get
            {
                return _interval;
            }
            set
            {
                if (value != _interval)
                {
                    _interval = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region GoBack

        public ICommand GoBack
        {
            get { return new DelegateCommand(DoGoBack); }
        }

        private void DoGoBack()
        {
            if (!HasChanged())
            {
                _homeNavigationFrame.Back();
                return;
            }

            if (Directory.Exists(FileLocation))
            {
                string title = Localization.Get(Localization.Key.Save);
                string message = Localization.Get(Localization.Key.QuestionSaveChanges);

                QuestionBox question = new QuestionBox(title, message);
                question.ViewModel.YesButtonClicked += ViewModel_YesButtonClicked;
                question.Closed += Question_Closed;
                question.Show(_parent);
            }
            else
            {
                //TODO remove hard coded text
                Message.ShowErrorMessage(_parent, "Save settings", $"File location '{FileLocation}' does not exist!");
            }
        }

        private void Question_Closed(object sender, EventArgs e)
        {
            _homeNavigationFrame.Back();
        }

        private void ViewModel_YesButtonClicked(object sender, QuestionBoxViewModel.YesButtonClickedEventArgs e)
        {
            SaveFields();
        }
        #endregion
        
        #region GoMaterialManagement

        public ICommand GoMaterialManagement
        {
            get { return new DelegateCommand(DoGoMaterialManagement); }
        }

        private void DoGoMaterialManagement()
        {
            // TODO get material list from db
            List<MaterialModel> list = new List<MaterialModel>();
            list.Add(new MaterialModel("Test"));
            list.Add(new MaterialModel("Test"));
            list.Add(new MaterialModel("Test"));
            list.Add(new MaterialModel("Test"));
            list.Add(new MaterialModel("Test"));

            _homeNavigationFrame.NavigateTo(new MaterialManagementView(_homeNavigationFrame, list));
        }
        #endregion        
    }
}
