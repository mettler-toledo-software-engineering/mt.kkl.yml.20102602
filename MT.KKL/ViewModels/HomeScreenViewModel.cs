﻿using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation;
using MT.Singularity.Platform.Devices;
using MT.KKL.Logic;

namespace MT.KKL.ViewModels
{
    /// <summary>
    /// Home Screen view model implementation
    /// </summary>
    public class HomeScreenViewModel : PropertyChangedBase
    {
        /// <summary>
        /// Constructor Initializes a new instance of the <see cref="HomeScreenViewModel"/> class.
        /// </summary>
        /// 

        private IInterfaceService _interfaces;
        public static HomeScreenViewModel Instance;
        private IScaleService _scaleService;

        private IScale _scale;

        public HomeScreenViewModel()
        {
            Instance = this;
            InitializeViewModel();
        }

        private async void InitializeViewModel()
        {
            var compositionContainer = ApplicationBootstrapperBase.CompositionContainer;
            var platformEngine = compositionContainer.Resolve<IPlatformEngine>();
            _interfaces = await platformEngine.GetInterfaceServiceAsync();

            _scaleService = await platformEngine.GetScaleServiceAsync();
            if (_scaleService == null)
                return;

            await _scaleService.GetAllScaleNumbersAsync();

            _scale = await _scaleService.GetScaleAsync(1);

            Globals.SetCurrentScale(_scale);
        }

        public async void InitializeScales()
        {
            var compositionContainer = ApplicationBootstrapperBase.CompositionContainer;

            var platformEngine = compositionContainer.Resolve<IPlatformEngine>();
            await platformEngine.GetScaleServiceAsync();
        }

        public IScale GetSelectedScale()
        {
            return _scaleService.SelectedScale;
        }

        #region WeightDisplay

        private int _weightDisplayHeight = 200;

        public int WeightDisplayHeight
        {
            get { return _weightDisplayHeight; }
            set
            {
                if (_weightDisplayHeight != value)
                {
                    _weightDisplayHeight = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region Visibility
        private Visibility _statusBarVisibility = Visibility.Collapsed;

        public Visibility StatusBarVisibility
        {
            get { return _statusBarVisibility; }
            set
            {
                if (_statusBarVisibility != value)
                {
                    _statusBarVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _weightDisplayShow;

        public Visibility WeightDisplayShow
        {
            get { return _weightDisplayShow; }
            set
            {
                if (_weightDisplayShow != value)
                {
                    _weightDisplayShow = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion
    }
}
