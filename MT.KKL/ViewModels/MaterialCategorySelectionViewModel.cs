﻿using log4net;
using MT.KKL.Views;
using MT.Singularity.Data;
using MT.Singularity.Logging;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;

namespace MT.KKL.ViewModels
{
    public class MaterialCategorySelectionViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        private static readonly ILog _log = Log4NetManager.ApplicationLogger;

        private DelegateCommand _goBackCommand;
        private DelegateCommand _goShowOverviewCommand;
        private DelegateCommand _goSelectFlammable;
        private DelegateCommand _goSelectMetallic;
        private DelegateCommand _goSelectNonFlammable;

        public MaterialCategorySelectionViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;
        }

        #region Buttons

        #region GoBack

        public ICommand GoBack => _goBackCommand = new DelegateCommand(DoGoBack);

        private void DoGoBack()
        {
            _homeNavigationFrame.Back();
        }

        #endregion

        #region GoShowOverview

        public ICommand GoShowOverview => _goShowOverviewCommand = new DelegateCommand(DoGoShowOverviewk);

        private void DoGoShowOverviewk()
        {
            _homeNavigationFrame.NavigateTo(new OverviewView(_homeNavigationFrame));
        }

        #endregion

        #region GoSelectFlammable
        public ICommand GoSelectFlammable => _goSelectFlammable = new DelegateCommand(DoGoSelectFlammable);

        private void DoGoSelectFlammable()
        {
            // TODO show only flammable materials
            _homeNavigationFrame.NavigateTo(new MaterialSelectionView(_homeNavigationFrame));
        }
        #endregion

        #region GoSelectMetallic
        public ICommand GoSelectMetallic => _goSelectMetallic = new DelegateCommand(DoGoSelectMetallic);

        private void DoGoSelectMetallic()
        {
            // TODO show only metallic materials
            _homeNavigationFrame.NavigateTo(new MaterialSelectionView(_homeNavigationFrame));
        }
        #endregion

        #region GoSelectNonFlammable
        public ICommand GoSelectNonFlammable => _goSelectNonFlammable = new DelegateCommand(DoGoSelectNonFlammable);

        private void DoGoSelectNonFlammable()
        {
            // TODO show only non flammable materials
            _homeNavigationFrame.NavigateTo(new MaterialSelectionView(_homeNavigationFrame));
        }
        #endregion

        #endregion

    }
}
