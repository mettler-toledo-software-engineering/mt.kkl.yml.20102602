﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using MT.KKL.Models;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;
using MT.KKL.Logic;
using log4net;
using MT.Singularity.Logging;
using MT.KKL.Views;
using MT.KKL.ViewModels.ControlViewModels;
using MT.KKL.Views.ControlViews;

namespace MT.KKL.ViewModels
{
    public class MaterialSelectionViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        
        private TransactionModel _transaction;
        private List<MaterialModel> _materialModels = new List<MaterialModel>();

        private DelegateCommand _materialCommand;
        private DelegateCommand _goBackCommand;
        private DelegateCommand _goShowOverviewCommand;

        public MaterialSelectionViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;

            _transaction = new TransactionModel();
                        
            GetAllMaterialsFromDB();
        }
        
        #region Material Command
        public ICommand GoGetMaterialList =>
            _materialCommand = new DelegateCommand(GetAllMaterialsFromDB, CanExecuteCommand);

        private bool CanExecuteCommand()
        {
            return true;
        }

        private async void GetAllMaterialsFromDB()
        {
            // TODO get materials from database
            //_materialModels = await DatabaseExtension.GetAllMaterialsFromDBAsync();

            if (_materialModels.Count == 0)
            {
                string title = Localization.Get(Localization.Key.Materiallist);
                string message = Localization.Get(Localization.Key.NoMaterialFound);

                Message.ShowErrorMessage(_parent, title, message);
            }
            else
            {
                MaterialButtons = new ObservableCollection<CtrlMaterialButtonView>();

                foreach (MaterialModel material in _materialModels)
                {
                    CtrlMaterialButtonView materialButton = new CtrlMaterialButtonView(_parent);
                    //materialButton.ViewModel.MaterialButtonClicked += ViewModel_MaterialButtonClicked;
                    MaterialButtons.Add(materialButton);
                }
            }
        }
        #endregion

        void ViewModel_MaterialButtonClicked(object sender, CtrlMaterialButtonViewModel.MaterialButtonClickedEventArgs e)
        {
            // TODO select material and go to weighing Window
            //_transaction.SetMaterial(e.Material);
            _homeNavigationFrame.NavigateTo(new WeighingView(_homeNavigationFrame));
        }

        #region Buttons
        
        #region MaterialButtons
        private ObservableCollection<CtrlMaterialButtonView> _materialButtons;

        public ObservableCollection<CtrlMaterialButtonView> MaterialButtons
        {
            get
            {
                return _materialButtons;
            }
            set
            {
                if (value != _materialButtons)
                {
                    _materialButtons = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region GoBack

        public ICommand GoBack => _goBackCommand = new DelegateCommand(DoGoBack);

        private void DoGoBack()
        {
            _homeNavigationFrame.Back();
        }

        #endregion

        #region GoShowOverview

        public ICommand GoShowOverview => _goShowOverviewCommand = new DelegateCommand(DoGoShowOverviewk);

        private void DoGoShowOverviewk()
        {
            _homeNavigationFrame.NavigateTo(new OverviewView(_homeNavigationFrame));
        }

        #endregion

        #endregion
    }
}
