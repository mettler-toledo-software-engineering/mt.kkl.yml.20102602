﻿using MT.KKL.Models;
using MT.Singularity.Data;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;
using MT.KKL.Data;
using MT.KKL.Logic;
using log4net;
using MT.Singularity.Logging;
using MT.KKL.Views.ChildViews;

namespace MT.KKL.ViewModels.ChildViewModels
{
    public class MaterialManagementViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        
        private DelegateCommand _materialAddCommand;
        private DelegateCommand _materialSaveCommand;

        public MaterialManagementViewModel(Visual parent, MaterialModel material, bool isNew)
        {
            _parent = parent;
            
            Title = isNew ? Localization.Get(Localization.Key.AddNewMaterial) : Localization.Get(Localization.Key.EditMaterial);
            SetButtonVisibility(!isNew);

            LoadFields(material);
        }

        private void LoadFields(MaterialModel material)
        {
            Material = material;
            
            Name = Material.Name;
        }

        private void SetButtonVisibility(bool showSaveButton)
        {
            AddVisibility = showSaveButton ? Visibility.Collapsed : Visibility.Visible;
            SaveVisibility = showSaveButton ? Visibility.Visible : Visibility.Collapsed;
        }

        private void CloseWindow()
        {
            ((MaterialManagement)_parent).Close();
        }

        #region Title
        private string _title;

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                if (value != _title)
                {
                    _title = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Material
        private MaterialModel _material;

        public MaterialModel Material
        {
            get
            {
                return _material;
            }
            set
            {
                if (value != _material)
                {
                    _material = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Code
        private string _code;

        public string Code
        {
            get
            {
                return _code;
            }
            set
            {
                if (value != _code)
                {
                    _code = value;
                    NotifyPropertyChanged();

                    _materialAddCommand?.NotifyCanExecuteChanged();
                    _materialSaveCommand?.NotifyCanExecuteChanged();
                }
            }
        }
        #endregion

        #region Name
        private string _name;

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value != _name)
                {
                    _name = value;
                    Material.Name = _name;
                    NotifyPropertyChanged();

                    _materialAddCommand?.NotifyCanExecuteChanged();
                    _materialSaveCommand?.NotifyCanExecuteChanged();
                }
            }
        }
        #endregion

        #region AddVisibility
        private Visibility _addVisibility;

        public Visibility AddVisibility
        {
            get
            {
                return _addVisibility;
            }
            set
            {
                if (value != _addVisibility)
                {
                    _addVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region SaveVisibility
        private Visibility _saveVisibility;

        public Visibility SaveVisibility
        {
            get
            {
                return _saveVisibility;
            }
            set
            {
                if (value != _saveVisibility)
                {
                    _saveVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Buttons

        #region GoCancel
        public ICommand GoCancel
        {
            get { return new DelegateCommand(DoGoCancel); }
        }

        private void DoGoCancel()
        {
            CloseWindow();
        }
        #endregion

        #region Save/Add

        private bool CanExecuteAddSaveMaterialCommand()
        {
            bool result = (string.IsNullOrWhiteSpace(Code) || string.IsNullOrWhiteSpace(Name)) == false;
            return result;
        }

        #region GoAdd
        public ICommand GoAdd =>
            _materialAddCommand = new DelegateCommand(AddMaterialCommandExecute, CanExecuteAddSaveMaterialCommand);

        private async void AddMaterialCommandExecute()
        {
            int result = await DatabaseExtension.SaveMaterialInDBAsync(Material);

            if (result == (int)SqlResult.Failed)
            {
                string title = Localization.Get(Localization.Key.AddNewMaterial);
                string message = Localization.Get(Localization.Key.AddMaterialFailed);

                Message.ShowErrorMessage(_parent, title, message);
            }
            else
            {
                CloseWindow();
            }
        }
        #endregion

        #region GoSave
        public ICommand GoSave =>
            _materialSaveCommand = new DelegateCommand(SaveMaterialCommandExecute, CanExecuteAddSaveMaterialCommand);

        private async void SaveMaterialCommandExecute()
        {
            bool result = await DatabaseExtension.UpdateMaterialInDBAsync(Material);

            if (!result)
            {
                string title = Localization.Get(Localization.Key.EditMaterial);
                string message = Localization.Get(Localization.Key.UpdateMaterialFailed);

                Message.ShowErrorMessage(_parent, title, message);
            }
            else
            {
                CloseWindow();
            }
        }
        #endregion

        #endregion

        #endregion
    }
}
