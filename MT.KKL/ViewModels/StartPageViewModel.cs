﻿using System;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Platform.Devices.Scale;
using MT.KKL.Logic;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.UserManagement;
using log4net;
using MT.Singularity.Logging;
using MT.KKL.Views;

namespace MT.KKL.ViewModels
{
    public class StartPageViewModel : PropertyChangedBase
    {
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(StartPageViewModel);

        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;
        
        private ISecurityService _securityService;
        private bool _hasPermission;
        private IScale _scale;
        private DelegateCommand _goToSettingsCommand;

        public StartPageViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame, ISecurityService securityService)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;
            _securityService = securityService;
            
            UpdateInformation();
            LoadDatabaseEntries();

            InitializeCurrentScale();
            InitializeSecurityServiceEvents();

            SetPermissionState();
        }

        private async void LoadDatabaseEntries()
        {
            // TODO
            //await DatabaseExtension.GetAllTransactionsFromDBAsync();
            //await DatabaseExtension.GetAllMaterialsFromDBAsync();
        }

        private void UpdateInformation()
        {
            ProjectNumber = Globals.ProjectNumber;
            Version = Globals.GetVersion();
        }

        #region Initialization

        private void InitializeSecurityServiceEvents()
        {
            _securityService.CurrentUserChanged -= _securityService_CurrentUserChanged;
            _securityService.CurrentUserChanged += _securityService_CurrentUserChanged;
        }
        
        private void InitializeCurrentScale()
        {
            _scale = Globals.GetCurrentScale();
        }

        #endregion
        
        #region Permission

        private void SetPermissionState()
        {
            _hasPermission = Permissions.IsSupervisorOrHigher(_securityService.CurrentUser.Permission);
        }

        private void _securityService_CurrentUserChanged(User user)
        {
            SetPermissionState();
            _goToSettingsCommand.NotifyCanExecuteChanged();
        }

        #endregion
        
        #region KeyHandling

        #region ---- GoContinue
        public ICommand GoContinue
        {
            get { return new DelegateCommand(DoGoContinue); }
        }

        private void DoGoContinue()
        {
            _homeNavigationFrame.NavigateTo(new MaterialCategorySelectionView(_homeNavigationFrame), TransitionAnimation.LeftToRight);
        }
        #endregion

        #region ---- GoSettings
        private void GoToSettingsCommandExecute()
        {
            _homeNavigationFrame.NavigateTo(new SettingsView(_homeNavigationFrame), TransitionAnimation.LeftToRight);
        }

        public ICommand GoSettings => _goToSettingsCommand = new DelegateCommand(GoToSettingsCommandExecute, CanExecuteGotToSettingsCommand);

        private bool CanExecuteGotToSettingsCommand()
        {
            return _hasPermission;
        }
        #endregion

        #region ---- Zero
        /// <summary>
        /// Zero
        /// </summary>
        public ICommand GoZero
        {
            get { return new DelegateCommand(DoGoZero); }
        }

        private void DoGoZero()
        {
            if (_scale != null)
            {
                _scale.ZeroAsync();
            }
        }
        #endregion

        #region ---- Tare
        /// <summary>
        /// Tare
        /// </summary>
        public ICommand GoTare
        {
            get { return new DelegateCommand(DoGoTare); }
        }

        private async void DoGoTare()
        {
            if (_scale != null)
            {
                WeightState state = await _scale.TareAsync();
                Console.WriteLine(state);
            }
        }
        #endregion

        #region ---- ClearTare
        /// <summary>
        /// ClearTare
        /// </summary>
        public ICommand GoClearTare
        {
            get { return new DelegateCommand(DoGoClearTare); }
        }

        private void DoGoClearTare()
        {
            if (_scale != null)
            {
                _scale.ClearTareAsync();
            }
        }
        #endregion

        #endregion

        #region Properties
        
        #region ---- ProjectNumber
        private string _projectNumber = "";
        public string ProjectNumber
        {
            get
            {
                return _projectNumber;
            }
            set
            {
                if (_projectNumber != value)
                {
                    _projectNumber = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region ---- Version
        private string _version = "";
        public string Version
        {
            get
            {
                return _version;
            }
            set
            {
                if (_version != value)
                {
                    _version = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion
        
        #endregion
    }
}
