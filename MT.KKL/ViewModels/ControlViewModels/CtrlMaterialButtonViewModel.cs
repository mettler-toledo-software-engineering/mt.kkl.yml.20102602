﻿
using MT.KKL.Models;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;
using System;

namespace MT.KKL.ViewModels.ControlViewModels
{
    public class CtrlMaterialButtonViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;

        public CtrlMaterialButtonViewModel(Visual parent)
        {
            _parent = parent;
        }


        #region Material
        private MaterialModel _material;

        public MaterialModel Material
        {
            get { return _material; }
            set
            {
                if (value != _material)
                {
                    _material = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Select

        public ICommand GoSelect
        {
            get { return new DelegateCommand(DoGoSelect); }
        }

        private void DoGoSelect()
        {
            OnMaterialButtonClicked(new MaterialButtonClickedEventArgs(Material));
        }

        #endregion

        #region Event MaterialButtonClicked

        public event EventHandler<MaterialButtonClickedEventArgs> MaterialButtonClicked;

        protected virtual void OnMaterialButtonClicked(MaterialButtonClickedEventArgs e)
        {
            if (MaterialButtonClicked != null)
                MaterialButtonClicked(this, e);
        }

        public class MaterialButtonClickedEventArgs : EventArgs
        {
            public MaterialModel Material { get; set; }

            public MaterialButtonClickedEventArgs(MaterialModel material)
            {
                Material = material;
            }
        }

        #endregion
    }
}
