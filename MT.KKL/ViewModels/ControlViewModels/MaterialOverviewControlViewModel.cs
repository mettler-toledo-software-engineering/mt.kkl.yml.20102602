﻿using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;

namespace MT.KKL.ViewModels.ControlViewModels
{
    public class MaterialOverviewControlViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;

        public MaterialOverviewControlViewModel(Visual parent)
        {
            _parent = parent;
        }
    }
}
