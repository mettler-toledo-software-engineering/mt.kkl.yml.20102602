﻿using log4net;
using MT.KKL.Models;
using MT.Singularity.Data;
using MT.Singularity.Logging;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;
using System;
using System.Collections.Generic;

namespace MT.KKL.ViewModels
{
    class MaterialManagementViewModel : PropertyChangedBase
    {
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(MaterialManagementViewModel);
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;
        private List<MaterialModel> _materials;

        private DelegateCommand _goBackCommand;
        private DelegateCommand _goNewCommand;

        public MaterialManagementViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame, List<MaterialModel> materials)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;
            _materials = materials;
        }

        #region Item Overview

        public virtual void LoadItems()
        {
            ItemList = new List<object>();
            //_materials = _currentProcess.MaterialList;

            if (_materials == null)
            {
                return;
            }

            foreach (MaterialModel material in _materials)
            {
                //MaterialModelControl modelControl = new MaterialModelControl(_homeNavigationFrame, material);
                //modelControl.ViewModel.MaterialItemChanged += ViewModel_MaterialItemChanged;
                //ItemList.Add(modelControl);
            }

            UpdateTitle();
        }

        //private void ViewModel_MaterialItemChanged(object sender, MaterialModelControlViewModel.MaterialItemChangedEventArgs e)
        //{
        //    LoadItems();
        //}

        public virtual void UpdateTitle()
        {
            Title = $"{Localization.Get(Localization.Key.Materiallist)} ({_materials.Count})";
        }

        #endregion

        #region Properties

        private string _title;

        public string Title
        {
            get { return _title; }
            set
            {
                if (value != _title)
                {
                    _title = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private List<object> _itemList;

        public List<object> ItemList
        {
            get { return _itemList; }
            set
            {
                if (value != _itemList)
                {
                    _itemList = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region Key Commands

        public virtual ICommand GoBack => _goBackCommand = new DelegateCommand(GoBackExecute);

        private void GoBackExecute()
        {
            _homeNavigationFrame.Back();
        }

        public virtual ICommand GoNew => _goNewCommand = new DelegateCommand(GoNewExecute);

        private void GoNewExecute()
        {
            // TODO go new

            //_materialManagement = new MaterialManagement(_homeNavigationFrame, new MaterialModel(), true);

            //if (_materialManagement != null)
            //{
            //    _materialManagement.Closed += _materialManagement_Closed;
            //    _materialManagement.Show(_parent);
            //}
        }

        //private void _materialManagement_Closed(object sender, System.EventArgs e)
        //{
        //    LoadItems();
        //}

        #endregion
    }
}
