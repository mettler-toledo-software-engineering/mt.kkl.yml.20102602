﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;
using MT.Singularity.Composition;
using MT.Singularity.Platform.CommonUX.Infrastructure;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.Infrastructure;
using log4net;
using MT.Singularity.Logging;
using MT.KKL.Config;

namespace MT.KKL.Infrastructure
{
    /// <summary>
    /// Initializes and runs the application.
    /// </summary>
    internal class Bootstrapper : ClientBootstrapperBase
    {
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(Bootstrapper);

        /// <summary>
        /// Initializes a new instance of the <see cref="Bootstrapper"/> class.
        /// </summary>
        public Bootstrapper()
            : base(Assembly.GetExecutingAssembly())
        {
        }

        /// <summary>
        /// Gets a value indicating whether it need to create customer database.
        /// </summary>
        protected override bool NeedToCreateCustomerDatabase
        {
            get { return true; }
        }
        
        protected override async void InitializeApplication()
        {
            await InitializeCustomerService().ContinueWith(InitializeCustomerServiceCompleted);
            base.InitializeApplication();
        }

        private async Task InitializeCustomerService()
        {
            try
            {
                var engine = CompositionContainer.Resolve<IPlatformEngine>();
                var securityService = await engine.GetSecurityServiceAsync();
                var configurationStore = CompositionContainer.Resolve<IConfigurationStore>();
                var customerComponent = new Components(configurationStore, securityService, CompositionContainer);

                CompositionContainer.AddInstance<IComponents>(customerComponent);
                await customerComponent.GetConfigurationAsync().ContinueWith(GetConfigurationCompleted);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void GetConfigurationCompleted(Task task)
        {
            if (task.Exception != null)
            {
                _log.ErrorEx("Get configuration failed!", _sourceClass);
                return;
            }

            _log.InfoEx("configuration completed", _sourceClass);
        }

        private void InitializeCustomerServiceCompleted(Task task)
        {
            if (task.Exception != null)
            {
                _log.ErrorEx("Initializing customer service failed!", _sourceClass);
                return;
            }

            _log.InfoEx("Customer service initialized", _sourceClass);
        }
    }
}
