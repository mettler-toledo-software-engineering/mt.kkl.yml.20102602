﻿
using MT.KKL.ViewModels;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls.Navigation;

namespace MT.KKL.Views
{
    /// <summary>
    /// Interaction logic for MaterialCategorySelectionView
    /// </summary>
    public partial class MaterialCategorySelectionView
    {
        private readonly MaterialCategorySelectionViewModel _viewModel;

        public MaterialCategorySelectionView(AnimatedNavigationFrame homeNavigationFrame)
        {
            _viewModel = new MaterialCategorySelectionViewModel(this, homeNavigationFrame);
            InitializeComponents();
        }

        protected override void OnFirstNavigation()
        {
            HomeScreenViewModel.Instance.WeightDisplayShow = Visibility.Collapsed;
            base.OnFirstNavigation();
        }
    }
}
