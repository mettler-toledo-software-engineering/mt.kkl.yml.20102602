﻿using MT.KKL.ViewModels;
using MT.Singularity.Presentation.Controls.Navigation;

namespace MT.KKL.Views
{
    /// <summary>
    /// Interaction logic for WeighingView
    /// </summary>
    public partial class WeighingView
    {
        private readonly WeighingViewModel _viewModel;

        public WeighingView(AnimatedNavigationFrame homeNavigationFrame)
        {
            _viewModel = new WeighingViewModel(this, homeNavigationFrame);
            InitializeComponents();
        }
    }
}
