﻿using MT.Singularity.Presentation.Controls.Navigation;
using MT.KKL.ViewModels;

namespace MT.KKL.Views
{
    /// <summary>
    /// Interaction logic for MaterialSelection
    /// </summary>
    public partial class MaterialSelectionView
    {
        private readonly MaterialSelectionViewModel _viewModel;

        public MaterialSelectionView(AnimatedNavigationFrame homeNavigationFrame)
        {
            _viewModel = new MaterialSelectionViewModel(this, homeNavigationFrame);
            InitializeComponents();
        }

        #region Overrides

        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {
            base.OnFirstNavigation();
        }

        /// <summary>
        /// Called when the user is navigating away from the current page to <paramref name="nextPage" />.
        /// </summary>
        /// <param name="nextPage">The next page the user is navigating to.</param>
        /// <returns>
        /// A value how the navigation framework should proceed with the navigation request.
        /// </returns>
        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {
            var result = base.OnNavigatingAway(nextPage);
            
            return result;
        }

        /// <summary>
        /// Called when a page is reactivated when returning from another page.
        /// </summary>
        /// <param name="previousPage">The page that the user is returning from.</param>
        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            base.OnNavigationReturning(previousPage);
        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            return base.OnNavigatingBack(nextPage);
        }

        #endregion 
    }
}
