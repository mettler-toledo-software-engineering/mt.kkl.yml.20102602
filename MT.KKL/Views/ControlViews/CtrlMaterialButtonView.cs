﻿using MT.KKL.ViewModels.ControlViewModels;
using MT.Singularity.Presentation.Controls;

namespace MT.KKL.Views.ControlViews
{
    /// <summary>
    /// Interaction logic for CtrlMaterialButtonView
    /// </summary>
    public partial class CtrlMaterialButtonView
    {
        private readonly CtrlMaterialButtonViewModel _viewModel;

        public CtrlMaterialButtonView(Visual parent)
        {
            _viewModel = new CtrlMaterialButtonViewModel(parent);

            InitializeComponents();
        }
    }
}
