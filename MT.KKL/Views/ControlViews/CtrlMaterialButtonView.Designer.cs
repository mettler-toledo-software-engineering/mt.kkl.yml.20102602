﻿using MT.Singularity.Presentation.Controls;
namespace MT.KKL.Views.ControlViews
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.2.0.0")]
    public partial class CtrlMaterialButtonView : MT.Singularity.Presentation.Controls.ContentControl
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.Button internal1;
            MT.Singularity.Presentation.Controls.GroupPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.StackPanel internal4;
            MT.Singularity.Presentation.Controls.TextBlock internal5;
            MT.Singularity.Presentation.Controls.StackPanel internal6;
            MT.Singularity.Presentation.Controls.TextBlock internal7;
            internal5 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal5.FontSize = ((System.Nullable<System.Int32>)28);
            internal5.Text = "Kunststoff halogenfrei";
            internal4 = new MT.Singularity.Presentation.Controls.StackPanel(internal5);
            internal4.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal7 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal7.FontSize = ((System.Nullable<System.Int32>)22);
            internal7.Text = "Max: 85 kg";
            internal6 = new MT.Singularity.Presentation.Controls.StackPanel(internal7);
            internal6.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal6.Margin = new MT.Singularity.Presentation.Thickness(0, 10, 0, 0);
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(internal4, internal6);
            internal3.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal3.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal3.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal2 = new MT.Singularity.Presentation.Controls.GroupPanel(internal3);
            internal1 = new MT.Singularity.Presentation.Controls.Button();
            internal1.Content = internal2;
            internal1.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal1.Width = 300;
            internal1.Height = 100;
            internal1.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal1.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal1.Command,() => _viewModel.GoSelect,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.Content = internal1;
            this.Margin = new MT.Singularity.Presentation.Thickness(10);
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[1];
    }
}
