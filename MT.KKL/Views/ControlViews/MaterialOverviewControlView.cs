﻿using MT.KKL.ViewModels.ControlViewModels;
using MT.Singularity.Presentation.Controls;

namespace MT.KKL.Views.Controlls
{
    /// <summary>
    /// Interaction logic for MaterialOverviewControll
    /// </summary>
    public partial class MaterialOverviewControlView
    {
        private readonly MaterialOverviewControlViewModel _viewModel;

        public MaterialOverviewControlView(Visual parent)
        {
            _viewModel = new MaterialOverviewControlViewModel(parent);

            //InitializeComponents();
        }
    }
}
