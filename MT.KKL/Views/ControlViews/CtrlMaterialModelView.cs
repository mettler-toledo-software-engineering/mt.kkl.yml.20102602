﻿using MT.KKL.ViewModels.ControlViewModels;
using MT.Singularity.Presentation.Controls;

namespace MT.KKL.Views.ControlViews
{
    /// <summary>
    /// Interaction logic for CtrlMaterialModelView
    /// </summary>
    public partial class CtrlMaterialModelView
    {
        private readonly CtrlMaterialModelViewModel _viewModel;

        public CtrlMaterialModelView(Visual parent)
        {
            _viewModel = new CtrlMaterialModelViewModel(parent);
            InitializeComponents();
        }
    }
}
