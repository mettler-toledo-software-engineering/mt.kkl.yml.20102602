﻿using MT.Singularity.Presentation.Controls;
namespace MT.KKL.Views.ControlViews
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.2.0.0")]
    public partial class CtrlMaterialModelView : MT.Singularity.Presentation.Controls.ContentControl
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.TextBlock internal3;
            MT.Singularity.Presentation.Controls.StackPanel internal4;
            MT.Singularity.Presentation.Controls.TextBlock internal5;
            MT.Singularity.Presentation.Controls.StackPanel internal6;
            MT.Singularity.Presentation.Controls.TextBlock internal7;
            MT.Singularity.Presentation.Controls.TextBlock internal8;
            MT.Singularity.Presentation.Controls.Button internal9;
            MT.Singularity.Presentation.Controls.Image internal10;
            MT.Singularity.Presentation.Controls.Button internal11;
            MT.Singularity.Presentation.Controls.Image internal12;
            internal3 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal3.FontSize = ((System.Nullable<System.Int32>)28);
            internal3.Width = 280;
            internal3.Text = "567678";
            internal5 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal5.Margin = new MT.Singularity.Presentation.Thickness(40, 0, 0, 0);
            internal5.Width = 630;
            internal5.FontSize = ((System.Nullable<System.Int32>)28);
            internal5.Text = "Karton";
            internal7 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal7.Margin = new MT.Singularity.Presentation.Thickness(40, 0, 0, 0);
            internal7.FontSize = ((System.Nullable<System.Int32>)18);
            internal7.Text = "Letzte Änderung:";
            internal8 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal8.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 0, 0);
            internal8.FontSize = ((System.Nullable<System.Int32>)18);
            internal8.Text = "25.11.2020 15:14";
            internal6 = new MT.Singularity.Presentation.Controls.StackPanel(internal7, internal8);
            internal6.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal6.Margin = new MT.Singularity.Presentation.Thickness(0, 10, 0, 0);
            internal4 = new MT.Singularity.Presentation.Controls.StackPanel(internal5, internal6);
            internal4.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal10 = new MT.Singularity.Presentation.Controls.Image();
            internal10.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal10.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal10.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal9 = new MT.Singularity.Presentation.Controls.Button();
            internal9.Content = internal10;
            internal9.Height = 60;
            internal9.Width = 80;
            internal9.Margin = new MT.Singularity.Presentation.Thickness(30, 0, 0, 0);
            internal12 = new MT.Singularity.Presentation.Controls.Image();
            internal12.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal12.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal12.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal11 = new MT.Singularity.Presentation.Controls.Button();
            internal11.Content = internal12;
            internal11.Height = 60;
            internal11.Width = 80;
            internal11.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 0, 0);
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal4, internal9, internal11);
            internal2.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 10, 0);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal2.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            internal1.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4293258224u));
            internal1.Width = 1200;
            internal1.Height = 80;
            this.Content = internal1;
        }
    }
}
