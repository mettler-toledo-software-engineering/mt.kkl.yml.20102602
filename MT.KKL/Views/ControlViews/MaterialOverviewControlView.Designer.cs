﻿using MT.Singularity.Presentation.Controls;
namespace MT.KKL.Views.ControlViews
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.2.0.0")]
    public partial class MaterialOverviewControllView : MT.Singularity.Presentation.Controls.ContentControl
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.TextBlock internal4;
            MT.Singularity.Presentation.Controls.StackPanel internal5;
            MT.Singularity.Presentation.Controls.TextBlock internal6;
            MT.Singularity.Presentation.Controls.StackPanel internal7;
            MT.Singularity.Presentation.Controls.TextBlock internal8;
            MT.Singularity.Presentation.Controls.StackPanel internal9;
            MT.Singularity.Presentation.Controls.TextBlock internal10;
            internal4 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal4.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal4.Margin = new MT.Singularity.Presentation.Thickness(5);
            internal4.Text = "Kunststoff halogenfrei";
            internal4.FontSize = ((System.Nullable<System.Int32>)18);
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(internal4);
            internal3.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4293263344u));
            internal3.Width = 300;
            internal3.Height = 30;
            internal3.Margin = new MT.Singularity.Presentation.Thickness(2, 2, 2, 0);
            internal6 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal6.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal6.Margin = new MT.Singularity.Presentation.Thickness(5);
            internal6.Text = "85.00 kg";
            internal6.FontSize = ((System.Nullable<System.Int32>)18);
            internal5 = new MT.Singularity.Presentation.Controls.StackPanel(internal6);
            internal5.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4293263344u));
            internal5.Width = 120;
            internal5.Height = 30;
            internal5.Margin = new MT.Singularity.Presentation.Thickness(0, 2, 2, 0);
            internal8 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal8.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal8.Margin = new MT.Singularity.Presentation.Thickness(5);
            internal8.Text = "";
            internal8.FontSize = ((System.Nullable<System.Int32>)18);
            internal7 = new MT.Singularity.Presentation.Controls.StackPanel(internal8);
            internal7.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4293263344u));
            internal7.Width = 710;
            internal7.Height = 30;
            internal7.Margin = new MT.Singularity.Presentation.Thickness(0, 2, 2, 0);
            internal10 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal10.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal10.Margin = new MT.Singularity.Presentation.Thickness(5);
            internal10.Text = "12.00 kg";
            internal10.FontSize = ((System.Nullable<System.Int32>)18);
            internal9 = new MT.Singularity.Presentation.Controls.StackPanel(internal10);
            internal9.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4293263344u));
            internal9.Width = 120;
            internal9.Height = 30;
            internal9.Margin = new MT.Singularity.Presentation.Thickness(0, 2, 2, 2);
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal5, internal7, internal9);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            internal1.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4278190080u));
            internal1.Margin = new MT.Singularity.Presentation.Thickness(10);
            this.Content = internal1;
            this.Margin = new MT.Singularity.Presentation.Thickness(10, -2, 10, -2);
        }
    }
}
