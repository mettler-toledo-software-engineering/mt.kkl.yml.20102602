﻿using MT.Singularity.Composition;
using MT.Singularity.Presentation;
using MT.Singularity.Platform.CommonUX.Infrastructure;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.KKL.Views;
using MT.Singularity.Platform.UserManagement;
using MT.Singularity.Platform.Infrastructure;
using MT.KKL.ViewModels;

namespace MT.KKL
{
    /// <summary>
    /// Interaction logic for HomeScreen
    /// </summary>
    [Export(typeof(IHomeScreenFactoryService))]
    public partial class HomeScreen : IHomeScreenFactoryService
    {
        private readonly HomeScreenViewModel _viewModel;

        private ISecurityService _securityService;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeScreen"/> class.
        /// </summary>
        public HomeScreen()
        {
            _viewModel = new HomeScreenViewModel();
            _securityService = ApplicationBootstrapperBase.CompositionContainer.Resolve<ISecurityService>();

            InitializeComponents();
            _viewModel.InitializeScales();

            homeNavigationFrame.NavigateTo(new StartPage(homeNavigationFrame, _securityService));
            _weightDisplayWindow.Activate();
            _viewModel.WeightDisplayShow = Visibility.Visible;
        }

        /// <summary>
        /// Gets the home screen page.
        /// </summary>
        /// <value>
        /// The home screen page.
        /// </value>
        public INavigationPage HomeScreenPage
        {
            get { return this; }
        }

        /// <summary>
        /// This method is called before the home screen is shown.
        /// </summary>
        public void BeforeStart(IRootVisualProvider rootVisualProvider)
        {
        }

        #region Overrides
        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            _viewModel.WeightDisplayShow = Visibility.Visible;
            return base.OnNavigatingBack(nextPage);
        }
        
        #endregion

        /// <summary>
        /// Gets a value indicating whether the cursor should be hidden.
        /// </summary>
        /// <value>
        ///   <c>true</c> to hide the cursor; otherwise, <c>false</c>.
        /// </value>
        public bool HideCursor
        {
            get { return false; }
        }

        public void SetWeightDisplaySize(int width, int height)
        {
            _weightDisplayWindow.Width = width;
            _weightDisplayWindow.Height = height;
        }
    }
}
