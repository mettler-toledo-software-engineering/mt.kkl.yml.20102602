﻿using MT.KKL.ViewModels;
using MT.Singularity.Presentation.Controls.Navigation;

namespace MT.KKL.Views
{
    /// <summary>
    /// Interaction logic for OverviewView
    /// </summary>
    public partial class OverviewView
    {
        private readonly OverviewViewModel _viewModel;

        public OverviewView(AnimatedNavigationFrame homeNavigationFrame)
        {
            _viewModel = new OverviewViewModel(this, homeNavigationFrame);
            InitializeComponents();
        }
    }
}
