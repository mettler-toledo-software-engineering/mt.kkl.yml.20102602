﻿using MT.Singularity.Presentation.Controls.Navigation;
using MT.KKL.Models;
using MT.KKL.ViewModels.ChildViewModels;

namespace MT.KKL.Views.ChildViews
{
    /// <summary>
    /// Interaction logic for MaterialManagement
    /// </summary>
    public partial class MaterialManagement
    {
        public MaterialManagementViewModel ViewModel;
        
        public MaterialManagement(MaterialModel material, bool isNew)
        {
            ViewModel = new MaterialManagementViewModel(this, material, isNew);
            InitializeComponents();
        }
    }
}
