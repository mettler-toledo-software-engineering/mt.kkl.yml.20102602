﻿using MT.KKL.ViewModels.ChildViewModels;

namespace MT.KKL.View.ChildViews
{
    /// <summary>
    /// Interaction logic for UserManagement
    /// </summary>
    public partial class QuestionBox
    {
        public readonly QuestionBoxViewModel ViewModel;

        public QuestionBox(string title, string message)
        {
            ViewModel = new QuestionBoxViewModel(this,title, message);
            InitializeComponents();
        }
    }
}
