﻿using MT.KKL.ViewModels.ChildViewModels;
using MT.Singularity.Presentation.Controls;

namespace MT.KKL.View.ChildViews
{
    /// <summary>
    /// Interaction logic for InfoBox
    /// </summary>
    public partial class InfoBox
    {
        public readonly InfoBoxViewModel ViewModel;

        public InfoBox(string title, string message)
        {
            ViewModel = new InfoBoxViewModel(this, title, message);

            InitializeComponents();
        }

        public InfoBox(string title, string message, MessageBoxIcon icon)
        {
            ViewModel = new InfoBoxViewModel(this, title, message, icon);

            InitializeComponents();
        }

        public InfoBox(string title, string message, MessageBoxIcon icon, MessageBoxButtons buttons)
        {
            ViewModel = new InfoBoxViewModel(this, title, message, icon, buttons);

            InitializeComponents();
        }
    }
}
