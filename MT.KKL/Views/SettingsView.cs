﻿using MT.KKL.ViewModels;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls.Navigation;

namespace MT.KKL.Views
{
    /// <summary>
    /// Interaction logic for Settings
    /// </summary>
    public partial class SettingsView
    {
        private readonly SettingsViewModel _viewModel;

        public SettingsView(AnimatedNavigationFrame homeNavigationFrame)
        {
            _viewModel = new SettingsViewModel(this, homeNavigationFrame);
            InitializeComponents();
        }

        #region Overrides

        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {
            HomeScreenViewModel.Instance.WeightDisplayShow = Visibility.Collapsed;
            base.OnFirstNavigation();
        }

        #endregion 
    }
}
