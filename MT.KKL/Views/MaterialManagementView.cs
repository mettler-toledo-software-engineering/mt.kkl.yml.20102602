﻿using MT.KKL.Models;
using MT.KKL.ViewModels;
using MT.Singularity.Presentation.Controls.Navigation;
using System.Collections.Generic;

namespace MT.KKL.Views
{
    /// <summary>
    /// Interaction logic for MaterialManagementView
    /// </summary>
    public partial class MaterialManagementView
    {
        private readonly MaterialManagementViewModel _viewModel;

        public MaterialManagementView(AnimatedNavigationFrame homeNavigationFrame, List<MaterialModel> models)
        {
            _viewModel = new MaterialManagementViewModel(this, homeNavigationFrame, models);
            InitializeComponents();
        }
    }
}
