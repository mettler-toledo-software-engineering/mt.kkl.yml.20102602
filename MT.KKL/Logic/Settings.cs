﻿using System;
using System.IO;
using System.Xml.Serialization;

#if IndPro
using MT.Singularity.Platform;
#endif

namespace MT.KKL.Logic
{
    [Serializable]
    public class Settings
    {
#if IndPro
        public static string FileName = Path.Combine(SingularityEnvironment.RunningApplication.DataDirectory, "Settings.xml");
#else
        public static string FileName = Path.Combine(".\\Settings", "Settings.xml");
#endif
        private static readonly XmlSerializer Serializer = new XmlSerializer(typeof(Settings));
        private static Settings _current;

        #region Load
        private static Settings Load()
        {
            if (!File.Exists(FileName))
            {
                return new Settings();
            }
            else
            {
                try
                {
                    using (FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        return (Settings)Serializer.Deserialize(fs);
                    }
                }
                catch (Exception)
                {
                    return new Settings();
                }
            }
        }

        public static Settings Current
        {
            get
            {
                EnsureSettings();
                return _current;
            }
        }

        public static void EnsureSettings()
        {
            if (_current == null)
                _current = Load();
        }
        #endregion

        public string FileLocation { get; set; }
        public int Interval { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Settings"/> class.
        /// </summary>
        public Settings()
        {
            FileLocation = "";
            Interval = 50000;
        }
        
        #region Save
        public void Save()
        {
            if (FileName == null) return;
            if (!Directory.Exists(FileName))
                Directory.CreateDirectory(Path.GetDirectoryName(FileName));

            using (var fs = new FileStream(FileName, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                Serializer.Serialize(fs, this);
            }
        }
        #endregion
    }
}