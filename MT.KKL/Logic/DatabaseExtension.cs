﻿using MT.KKL.Data.Database;
using MT.KKL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransactionModel = MT.KKL.Models.TransactionModel;

namespace MT.KKL.Logic
{
    public static class DatabaseExtension
    {
        private static IDataBaseConnection<TransactionModel> _transactionModelBaseConnection = new SqliteGenericDataBaseAccess<TransactionModel>();
        private static IDataBaseConnection<MaterialModel> _materialModelBaseConnection = new SqliteGenericDataBaseAccess<MaterialModel>();
        
        private static List<MaterialModel> _materialModels = new List<MaterialModel>();
        private static List<TransactionModel> _transactionModels = new List<TransactionModel>();

        private static DateTime CurrentDateTime()
        {
            return DateTime.Now;
        }

        #region Material
        public static List<MaterialModel> GetMaterials()
        {
            return _materialModels;
        }

        public static async Task<List<MaterialModel>> GetAllMaterialsFromDBAsync()
        {
            _materialModels = await _materialModelBaseConnection.GetAllDataEntriesAsync();

            return _materialModels;
        }

        private static bool IsUsedMaterial(int id)
        {
            bool hasMaterialId = false;

            _transactionModels.ForEach(m =>
            {
                if (m.MaterialId == id)
                {
                    hasMaterialId = true;
                }
            });

            return hasMaterialId;
        }

        public static async Task<bool> DeleteMaterialFromDBAsync(MaterialModel material)
        {
            if (!IsUsedMaterial(material.Id))
            {
                bool result = await _materialModelBaseConnection.DeleteDataAsync(material);
                await GetAllMaterialsFromDBAsync();

                return result;
            }

            return false;
        }

        public static async Task<int> SaveMaterialInDBAsync(MaterialModel material)
        {
            material.TimeStamp = CurrentDateTime();
            material.LastChanged = CurrentDateTime();

            int result = await _materialModelBaseConnection.SaveDataAsync(material);
            await GetAllMaterialsFromDBAsync();

            return result;
        }

        public static async Task<bool> UpdateMaterialInDBAsync(MaterialModel material)
        {
            material.LastChanged = CurrentDateTime();

            bool result = await _materialModelBaseConnection.UpdateDataAsnyc(material);
            await GetAllMaterialsFromDBAsync();

            return result;
        }
        #endregion
        
        #region Transaction

        public static List<TransactionModel> GetTransactions()
        {
            return _transactionModels;
        }

        public static async Task<List<TransactionModel>> GetAllTransactionsFromDBAsync()
        {
            _transactionModels = await _transactionModelBaseConnection.GetAllDataEntriesAsync();

            if (_transactionModels.Count == 0)
            {
                return null;
            }
            else
            {
                foreach (TransactionModel transaction in _transactionModels)
                {
                    if (transaction.Material == null)
                    {
                        var material = await _materialModelBaseConnection.GetEntryByIdAsync(transaction.MaterialId);
                        if (material != null)
                        {
                            transaction.SetMaterial(material);
                        }
                    }
                }
            }

            return _transactionModels;
        }

        public static async Task<int> SaveTransactionInDBAsync(TransactionModel transaction)
        {
            transaction.TimeStamp = CurrentDateTime();
            transaction.IsExported = false;

            int result = await _transactionModelBaseConnection.SaveDataAsync(transaction);
            await GetAllTransactionsFromDBAsync();

            return result;
        }
        #endregion
    }
}
