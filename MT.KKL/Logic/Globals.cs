﻿using MT.Singularity.Platform;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation;
using System.Configuration;
using System.IO;
using System.Reflection;
using MT.KKL.ViewModels;
using MT.Singularity;

namespace MT.KKL.Logic
{
    public static class Globals
    {
        private static IScale _scale;
        private static readonly SingularityEnvironment Environment = new SingularityEnvironment("MT.KKL");

        public static string BarcodeReaderName = "Cino FuzzyScan";
        public static int MessageShowInterval = 2000;
        public static string ProjectNumber = "P20102602";

        public static readonly string ControlBackground = "#FFE5EBF0";
        public static readonly Color ControlBackgroundColor = new Color(0,229,235,240);

        public static readonly string IND930USBDrive = "D:\\";
        public static string UsbBackupBaseDirectory = Path.Combine(IND930USBDrive, "Backup");
        public static string SystemUpdateSourceDirectory = Path.Combine(IND930USBDrive, "systemupdate");

        public static readonly string SelectedControlBackground = "#FF7F9BB5";
        public static readonly Color SelectedControlBackgroundColor = new Color(0, 127, 155, 181);

        public static string GetDataDirectory()
        {
            return Environment.DataDirectory;
        }

        public static string ConnectionString()
        {
            //um den Configuration Manager nutzen zu können muss die Referenz System.Configuration hinzugefügt werden
            //add reference --> assemblies-- > system.configuration
            return ConfigurationManager.ConnectionStrings["KKLSqlite"].ConnectionString;
        }

        public static string GetVersion()
        {
            return "V " + Assembly.GetExecutingAssembly().GetName().Version.Major +
                   "." + Assembly.GetExecutingAssembly().GetName().Version.Minor +
                   "." + Assembly.GetExecutingAssembly().GetName().Version.Build;
        }

        public static void SetCurrentScale(IScale scale)
        {
            _scale = scale;
        }

        public static IScale GetCurrentScale()
        {
            return _scale;
        }

        public static string GetCurrentScaleUnit()
        {
            return _scale.CurrentDisplayUnit.ToAbbreviation();
        }

        public static void SetWeightDisplayHeight(int height)
        {
            HomeScreenViewModel.Instance.WeightDisplayHeight = height;
        }

        public static void SetWeightDisplayStandardHeight()
        {
            HomeScreenViewModel.Instance.WeightDisplayHeight = 200;
        }
    }
}
