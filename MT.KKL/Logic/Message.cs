﻿using MT.KKL.View;
using MT.KKL.View.ChildViews;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Utils;

namespace MT.KKL.Logic
{
    public static class Message
    {
        private static InfoBox _info;
        private static Timer _timer;

        public static void ShowErrorMessage(Visual parent, string title, string message)
        {
            _info = new InfoBox(title, message, MessageBoxIcon.Error, MessageBoxButtons.OK);
            _info.Show(parent);
        }

        public static void ShowInfoMessage(Visual parent, string title, string message)
        {
            _info = new InfoBox(title, message, MessageBoxIcon.Information, MessageBoxButtons.OK);
            _info.Show(parent);
        }

        public static void ShowInfoMessage(Visual parent, string title, string message, int seconds)
        {
            _info = new InfoBox(title, message, MessageBoxIcon.Information);
            _info.Show(parent);

            _timer = new Timer();
            _timer.Interval = seconds;
            _timer.Tick += TimerTick;
            _timer.Enabled = true;
        }

        private static void TimerTick(object sender, System.EventArgs e)
        {
            _timer.Tick -= TimerTick;
            _timer.Enabled = false;
            
            _info.Close();
        }
    }
}
