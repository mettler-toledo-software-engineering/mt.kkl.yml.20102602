﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using MT.Singularity.Logging;

namespace MT.Balancer.Logic
{
    public class StepController
    {

        public List<StepModel> Steps { get; set; }
        private readonly XmlSerializer serializer = new XmlSerializer(typeof(StepController));

        public StepController()
        {

        }

        public void Save()
        {
            string xmlData = SerializeClass<StepController>.Serialize(this);
            File.WriteAllText(@"C:\temp\DocuLists.xml", xmlData);

        }

        public StepController Load()
        {
            string xmlData;
            using (var sr = new StreamReader(@"C:\temp\DocuList.xml", System.Text.Encoding.Default))
            {
                xmlData = sr.ReadToEnd();
                try
                {
                    return SerializeClass<StepController>.Deserialize(xmlData);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    Log4NetManager.ApplicationLogger.Error("Error StepController.Load:", ex);
                    return null;
                }

            }

        }

    }
}
