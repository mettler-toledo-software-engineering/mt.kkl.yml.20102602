﻿using CsvHelper;
using log4net;
using MT.KKL.Models;
using MT.Singularity.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MT.KKL.Logic
{
    public static class Extensions
    {
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(Extensions);
        
        public static List<CsvModel> ConvertTransactionModelForCsv(List<TransactionModel> transactions)
        {
            try
            {
                return transactions
                    .Select(model => new CsvModel()
                    {
                        NetWeight = model.NetWeight,
                        TareWeight = model.TareWeight,
                        GrossWeight = model.GrossWeight,
                        Unit = model.Unit
                    })
                    .ToList();
            }
            catch (Exception ex)
            {
                _log.ErrorEx("converting transaction models as csv models failed", _sourceClass, ex);

                return new List<CsvModel>();
            }
        }

        public static bool WriteTransactionsToFileSystemAsCsv(List<CsvModel> transactions)
        {
            int digits = 5;

            string path = Settings.Current.FileLocation;
            string fileName = $"Transactions-{DateTime.Now:yyyy-MM-dd HH-mm-ss}.txt";
            string destination = Path.Combine(path, fileName);

            try
            {
                using (var writer = new StreamWriter(destination))
                using (var csv = new CsvWriter(writer))
                {
                    foreach (var transaction in transactions)
                    {
                        transaction.TareWeight = Math.Round(transaction.TareWeight, digits);
                        transaction.NetWeight = Math.Round(transaction.NetWeight, digits);
                        transaction.GrossWeight = Math.Round(transaction.GrossWeight, digits);

                    }
                    csv.Configuration.Delimiter = ";";
                    csv.WriteRecords(transactions);
                    
                    _log.Success("create txt file with transactions from database", _sourceClass);
                }

                return true;
            }
            catch (Exception ex)
            {
                _log.ErrorEx("writing transactions to txt file failed", _sourceClass, ex);

                return false;
            }
        }
    }
}
