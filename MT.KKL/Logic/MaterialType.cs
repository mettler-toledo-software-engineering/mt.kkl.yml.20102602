﻿namespace MT.KKL.Logic
{
    public enum MaterialType
    {
        Metallic,
        Flammable,
        NonFlammable
    }
}
